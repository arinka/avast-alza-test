//Test Case Steps
// 1. Go to Alza.cz
// 2. Go to TVs
// 3. Choose order from most expensive
// 4. Choose the first TV in a list and go to its description page
// 5. Click "Buy" button
// 6. Close the dialog with additional offers
// 7. Go to the basket
// 8. Check for item to be in the basket

const tvTitle = '98" Sony Bravia KD-98ZG9'

Cypress.on('uncaught:exception', (err, runnable) => {
    return false
})
        
function get_nth_item(item_number) {
    return cy.get(`#boxes > div:nth-child(${item_number})`)
}

describe('Add the most expensive TV to the cart', () => {
    
    it('Full test', () => {
        cy.log("visit alza.cz")
        cy.visit("https://www.alza.cz")
            .title()
            .should('eq', "Alza.cz - největší obchod s počítači a elektronikou | Alza.cz")
        
        cy.log("go to TVs")
        cy.get('a').contains('Televize').click()

        cy.log("check if we are in the right location")
        cy.location('href').should('eq', 'https://www.alza.cz/televize/18849604.htm')

        cy.wait(3000)

        cy.log('select order type "From the most expensive"')
        cy.get('#ui-id-4').contains('Od nejd').click()

        cy.log("check if we are in the right location")
        cy.location('href').should('eq', 'https://www.alza.cz/luxusni-nejdrazsi-televize/18849604.htm')

        cy.wait(3000)

        cy.log("go to the most expensive TV description page")
        get_nth_item(1).children('div:first-child').children('.fb').contains('a').first().click()
        
        cy.log("descriprion page should have the same tv name as previous page")
        cy.get('h1').invoke('text').should((title) => {
            expect(title).to.contain(tvTitle)
        })
        
        cy.log("buy TV")
        cy.get('.btnx').contains("Koupit").click()

        cy.wait(3000)

        cy.log("close the dialog with additional offers")
        cy.get('#alzaDialog > div.close').click()

        cy.wait(3000)

        cy.log("go to the basket")
        cy.get('a#varAToBasketButton').click()
        
        cy.log("check if we are in the right location")
        cy.location('href').should('eq', 'https://www.alza.cz/Order1.htm')

        cy.log("table with TV should exist")
        cy.get('table.o1grid').should('be.visible')

        cy.log("title should be equal to chosen TV")
        cy.get('a.mainItem').first().invoke('text').should((title) => {
            expect(title).to.contain(tvTitle)
        })
    })
  })