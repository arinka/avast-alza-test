# Alza test

This is a solution that is running a simple test against alza store

## How to launch it?

Just run the following commands in cmd against the project folder
```bash
npm install
npm run alza_test
```